//
//  HomeViewController.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak private var tblMovies : UITableView!
    
    private var arrayGeneral = [Any](){
        didSet{
            self.tblMovies.reloadSections(IndexSet(arrayLiteral: 0), with: .bottom)
        }
    }
    
    lazy private var refeshControll : UIRefreshControl! = {
        let objRefresh = UIRefreshControl()
        objRefresh.tintColor = .black
        objRefresh.addTarget(self, action: #selector(self.getMovies), for: .valueChanged)
        return objRefresh
    }()

    @IBAction private func btnExitSession(_ sender : UIButton?){
        SessionBL.sharedSession.deleteSession()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadHome()
        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let controller = segue.destination as? ThreeViewController{
//            controller.objMensaje = sender as? String
        }
    }
    

}

//MARK: - METHODS
extension HomeViewController {
    
    private func loadHome(){
        self.tblMovies.delegate = self
        self.tblMovies.dataSource = self
        self.tblMovies.backgroundView = self.refeshControll
        self.getMovies()
    }
    
    @objc private func getMovies(){
        self.refeshControll.beginRefreshing()
        PeliculasBL.shared.getAllMovies { objPeliculaBE in
            self.refeshControll.endRefreshing()
            self.arrayGeneral = objPeliculaBE.peliculas_resul ?? ["Sin datos"]
        } errorService: { errorMessage in
            self.refeshControll.endRefreshing()
            self.arrayGeneral = [errorMessage]
        }
    }
}


//MARK: - TableViewDelegate, TableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayGeneral.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrayGeneral is [PeliculaResultBE]{
            return self.tableViewMoview(tableView, cellForRowAt: indexPath)
        }else{
            return self.tableViewErrorMessage(tableView, cellForRowAt: indexPath)
        }
    }
    
    func tableViewMoview(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "HomeTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HomeTableViewCell
        cell.objMovie = self.arrayGeneral[indexPath.row] as? PeliculaResultBE
        return cell
    }
    
    func tableViewErrorMessage(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ErrorMessageTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ErrorMessageTableViewCell
        cell.message = self.arrayGeneral[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrayGeneral is [PeliculaResultBE]{
            return UITableView.automaticDimension
        }else{
            return self.tblMovies.frame.height
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ThreeViewController", sender: "Pagina 3")
    }
}



class ThreeViewController : UIViewController {
 
    @IBOutlet weak private var lblName : UILabel!
    
    var arrayName : [ String]?
    var lstNAmes : String?
    
    private var objMensjeName : String?
    
    override func viewDidLoad() {
        
    }
    
}
