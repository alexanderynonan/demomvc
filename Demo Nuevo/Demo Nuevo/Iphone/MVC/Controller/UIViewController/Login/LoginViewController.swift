//
//  LoginViewController.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak private var txtUser      : UITextField!
    @IBOutlet weak private var txtPassword  : UITextField!
    @IBOutlet weak private var activityUser : UIActivityIndicatorView!
    
    private var objUser = UserBE()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction private func btnLoginSession(_ sender : UIButton?){
        self.getLogin()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: -METHODS
extension LoginViewController {
    
    private func loadLogin(){
        
    }
    
    private func getLogin(){
        
        self.objUser.user = self.txtUser.text
        self.objUser.password = self.txtPassword.text
        self.animateLogin(show: true)
        
        UserBL.shared.loginSession(objUser: self.objUser) {
            self.animateLogin(show: false)
            self.setearLogin()
        } errorService: { errorMessage in
            print(errorMessage)
            self.animateLogin(show: false)
            
            self.setearLogin() //SETEO POR QUE NO CUENTO CON SERVICIO LOGIN
        }
    }
    
    private func setearLogin(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
            
            let obj = UserBE()
            obj.name = self.objUser.user
            obj.lastName = self.objUser.user
            obj.email = self.objUser.user
            
            SessionBL.sharedSession.saveSession(obj: obj)
            
            self.performSegue(withIdentifier: "HomeViewController", sender: nil)
        }
    }
    
    private func animateLogin(show: Bool){
        self.view.isUserInteractionEnabled = !show
        show ? self.activityUser.startAnimating() : self.activityUser.stopAnimating()
    }
}
