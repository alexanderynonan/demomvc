//
//  SplashViewController.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak private var activityUser : UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityUser.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
        self.loadDidApper()
    }

}

//MARK: -METHODS
extension SplashViewController {
    
    private func loadDidApper(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.activityUser.stopAnimating()
            
            if let _ = SessionBL.sharedSession.getSession(){
                self.performSegue(withIdentifier: "HomeViewController", sender: nil)
            }else{
                self.performSegue(withIdentifier: "LoginViewController", sender: nil)
            }
        }
    }
}
