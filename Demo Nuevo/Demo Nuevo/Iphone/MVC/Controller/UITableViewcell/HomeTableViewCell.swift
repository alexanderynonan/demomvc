//
//  HomeTableViewCell.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak private var lblTitle  : UILabel!
    @IBOutlet weak private var lblDescripcion  : UILabel!
    @IBOutlet weak private var imgMoview : UIImageView!
    @IBOutlet weak private var activityMoview : UIActivityIndicatorView!
    
    var objMovie : PeliculaResultBE!{
        didSet{
            self.lblTitle.text = self.objMovie.original_title
            self.lblDescripcion.text = self.objMovie.overview
            
            self.activityMoview.startAnimating()
            
            self.imgMoview.downloadImageView(url: self.objMovie.image_poster) { urlImage, image in
                
                self.activityMoview.stopAnimating()
                
                if self.objMovie.image_poster == urlImage{
                    self.imgMoview.image = image
                }else{
                    self.imgMoview.image = nil
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
