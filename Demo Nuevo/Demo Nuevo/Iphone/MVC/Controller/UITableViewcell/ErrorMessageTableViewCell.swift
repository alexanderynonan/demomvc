//
//  ErrorMessageTableViewCell.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

class ErrorMessageTableViewCell: UITableViewCell {

    @IBOutlet weak private var lblTitle  : UILabel!
    
    var message : String!{
        didSet{
            self.lblTitle.text = self.message
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
