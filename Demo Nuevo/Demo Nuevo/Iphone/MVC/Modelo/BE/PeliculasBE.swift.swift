//
//  PeliculasBE.swift.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation
import SwiftyJSON

struct PeliculaBE {
    
    let page : Int?
    let total_pages : Int?
    let total_results : Int?
    let peliculas_resul : [PeliculaResultBE]?
    
    init(json : JSON? = nil) {
        self.page = json?.dictionary?["page"]?.int
        self.peliculas_resul = json?.dictionary?["results"]?.array?.map({ PeliculaResultBE(json: $0) }) ?? []
        self.total_pages = json?.dictionary?["total_pages"]?.int
        self.total_results = json?.dictionary?["total_results"]?.int
    }
}

struct PeliculaResultBE {
    
    let id: Int?
    let poster_path: String?
    let release_date: String?
    let title: String?
    let vote_average: Double?
    let overview: String?
    let original_title : String?
    let gere_ids : [Int]?
    
    init(json : JSON? = nil) {
        self.id = json?.dictionary?["id"]?.int
        self.original_title = json?.dictionary?["original_title"]?.string
        self.poster_path = json?.dictionary?["poster_path"]?.string
        self.release_date = json?.dictionary?["release_date"]?.string
        self.title = json?.dictionary?["title"]?.string
        self.vote_average = json?.dictionary?["vote_average"]?.double
        self.overview = json?.dictionary?["overview"]?.string
        self.gere_ids = json?.dictionary?["genre_ids"]?.array?.map({ $0.int ?? 0 }) ?? []
    }
    
    var imageFinal : String {
        return "https://image.tmdb.org/t/p/w500\(self.poster_path ?? "")"
    }
}
