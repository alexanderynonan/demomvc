//
//  UserBE.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

class UserBE {
    
    var user        : String?
    var password    : String?
    var name        : String?
    var lastName    : String?
    var email       : String?
    
    var dic_login : [String : Any] {
        
        let dic : [String : Any] = ["user" : self.user ?? "",
                                    "password" : self.password ?? ""]
        return dic
    }
    
}
