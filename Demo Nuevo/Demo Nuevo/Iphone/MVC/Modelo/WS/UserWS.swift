//
//  UserWS.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

class UserWS {
    
    static let shared = UserWS()
    
    func getLogin(dic params : [String : Any],completionService : @escaping Closures.Success,errorService : @escaping Closures.ErrorMessage){
        
        let url = WebServicesURL.LoginURL.login
        
        CSWebservice.sharedInstace.requestType(type: .post, url: url, params: params, header: nil) { response in

            CSWebservice.sharedInstace.genericResponse(response) { json in
                
                completionService()
                
            } error: { errorMessage in
                errorService(errorMessage)
            }
        }
    }
    
}
