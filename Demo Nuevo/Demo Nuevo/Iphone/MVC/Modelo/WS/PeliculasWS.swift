//
//  PeliculasWS.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation
import Alamofire

struct PeliculasWS {
    
    static let shared = PeliculasWS()
    
    func getAllMovies(completionService : @escaping Closures.Pelicula,errorService : @escaping Closures.ErrorMessage){
        
        let url = WebServicesURL.PeliculaURL.getMovies
        
//        let dic : [String : Any] = ["Id" : 120012 , "name" : "Alexander", "token" : "sd1@#q98as!asid=asd1" ] //EN CASO ENVIEN PARAMETROS
        
        CSWebservice.sharedInstace.requestType(type: .get, url: url, params: nil , header: nil) { response in
            
            CSWebservice.sharedInstace.genericResponse(response) { json in
                
                completionService(PeliculaBE(json: response))
                
            } error: { errorMessage in
                errorService(errorMessage)
            }
        }
    }
    
}
