//
//  CSWebService.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation
import Alamofire
import SwiftyJSON

class CSWebservice {
    
    static let sharedInstace = CSWebservice()
    
    func requestType(type serviceType : HTTPMethod,url : String,params : Parameters? = nil,header : HTTPHeaders? = nil,successResponse : @escaping Closures.SuccessResponse){
        
        let request = AF.request(url, method: serviceType,parameters: params, headers: header)
        request.response { response in
            
            print("\n\n**********************************************************************")
            print("\nPARAMETERS: \(JSON(params ?? "Sin Parametros")) \n")
            print("SERVICIO \(serviceType.rawValue): \(url)")
            print(JSON(response.data ?? MessageAlert.General.errorServiceNotFount))
            successResponse(JSON(response.data ?? ""))
            print("**********************************************************************\n\n")
        }
    }

    
//    MARK: - VALIDATION RESPONSE DATA DEFAULTS
    func genericResponse(_ response: JSON,success: (_ json: JSON) -> Void, error: @escaping Closures.FailureResponse){
        
//        if response.dictionary?["status"]?.int == 200 || response.dictionary?["status"]?.string == "200"{
            if let data = response.dictionary?["results"]{
                success(data)
            }else{
                error(MessageAlert.General.errorServiceParse)
            }
//        }else{
//            error(self.manageErrorFromResponse(response))
//        }
    }
    
//    MARK: - VALIDATION RESPONSE DATA DEFAULTS
    
    func responseStatus(_ response: JSON,_ success: @escaping Closures.Message , error: @escaping Closures.ErrorMessage){

        if response.dictionary?["status"]?.int == 200 || response.dictionary?["status"]?.string == "200"{
            success(self.manageErrorFromResponse(response))
        }else{
            error(self.manageErrorFromResponse(response))
        }
    }

    
//    MARK: - Validation Responde Message
    
    private func manageErrorFromResponse(_ response: JSON? = nil) -> String{
        if let message = response?.dictionary?["message"]{
            return message.stringValue
        }else if let message = response?.dictionary?["mensaje"]{
            return message.stringValue
        }else{
            return MessageAlert.General.errorService
        }
   }
}
