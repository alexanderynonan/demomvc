//
//  SessionBL.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

//MARK: - GUARDAR SESSION CON USER DEFAULT
class SessionBL {
    
    static var sharedSession = SessionBL()
    
//    Obtner session con datos guardados del login
    func getSession() -> UserBE? {
        
        let defaults = UserDefaults.standard

        if let name     = defaults.value(forKey: "name") as? String,
           let lastName = defaults.value(forKey: "lastName") as? String,
           let email    = defaults.value(forKey: "email") as? String{
            
            let obj         = UserBE()
            obj.name        = name
            obj.lastName    = lastName
            obj.email       = email
            
            return obj            
        }else{
            return nil
        }
    }
    
//  Guardar session
    func saveSession(obj : UserBE){
        
        self.deleteSession()
        
        let defaults = UserDefaults.standard
        defaults.setValue(obj.name, forKey: "name")
        defaults.setValue(obj.lastName , forKey: "lastName")
        defaults.setValue(obj.email , forKey: "email")
        defaults.synchronize()
    }

//  ELIMINAR session
    func deleteSession(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "name")
        defaults.removeObject(forKey: "lastName")
        defaults.removeObject(forKey: "email")
        defaults.synchronize()
    }
    
}


