//
//  UserBL.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

struct UserBL {
 
    static let shared = UserBL()
    
    
    func loginSession(objUser : UserBE?,completionService : @escaping Closures.Success, errorService : @escaping Closures.ErrorMessage) {
        
        if objUser?.user?.count ?? 0 == 0{
            errorService(MessageAlert.Login.errorUser)
            return
        }
        
        if objUser?.password?.count ?? 0 <= 5{
            errorService(MessageAlert.Login.errorPassword)
            return
        }
                
        UserWS.shared.getLogin(dic: objUser!.dic_login, completionService: completionService, errorService: errorService)        
    }
    
}
