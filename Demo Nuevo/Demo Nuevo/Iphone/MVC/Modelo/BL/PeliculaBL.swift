//
//  PeliculaBL.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

struct PeliculasBL {
 
    static let shared = PeliculasBL()
    
    func getAllMovies(completionService : @escaping Closures.Pelicula, errorService : @escaping Closures.ErrorMessage) {
        
        if let _ = SessionBL.sharedSession.getSession(){
            PeliculasWS.shared.getAllMovies(completionService: completionService, errorService: errorService)
        }else{
            errorService(MessageAlert.General.sessionExpire)
        }
    }
    
}
