//
//  WebServicesURL.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

struct WebServicesURL {
    
    static let baseURLServices = "https://api.themoviedb.org/3/movie/"
    static let urlJSON         = "https://app.peruapps.com/json/"
    
    struct LoginURL {
        static let login          = baseURLServices + "login"
    }
    
    struct PeliculaURL {
        static let getMovies      = baseURLServices + "popular?api_key=176de15e8c8523a92ff640f432966c9c"
    }
    
    
}
