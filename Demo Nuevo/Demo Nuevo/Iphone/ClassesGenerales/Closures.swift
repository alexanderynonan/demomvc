//
//  Closures.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

import SwiftyJSON

struct Closures {
    
    //MARK: General
    typealias ErrorMessage              = (_ errorMessage   : String)           -> Void
    typealias Message                   = (_ message        : String)           -> Void
    typealias Success                   = () -> Void

    //MARK: WebServices
    typealias SuccessResponse           = (_ response       : JSON)             -> Void
    typealias FailureResponse           = (_ errorMessage   : String)           -> Void

    //MARK: Peliculas
    typealias Pelicula                  = (_ objPeliculaBE  : PeliculaBE)   -> Void
    
    typealias ImageDownload             = (_ urlImage : String,_ image : UIImage)   -> Void
}



