//
//  ExtensionViewController.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit

extension UIViewController{

    @IBAction public func btnExitGeneral(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}
