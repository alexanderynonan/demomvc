//
//  WebServicesImage.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import UIKit
import Alamofire

extension UIImageView {

    func downloadImageView(url : String, completionImage : @escaping Closures.ImageDownload){
        
        self.image = nil
        
        let request = AF.request(url, method: .get)
        request.response { response in
            
            if response.response?.url?.absoluteString == url {
                
                guard let data = response.data else {
                    completionImage(url,UIImage())
                    return
                }
                
                guard let image = UIImage(data: data) else {
                    completionImage(url,UIImage())
                    return
                }
                completionImage(url,image)                
            }
        }
    }
}
