//
//  Constantes.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

struct MessageAlert {
    
    struct General {
        static let title                    = "Demo Nuevo" //Nombre del app
        static let upsButton                = "UPS!"
        static let sessionExpire            = "Sesión caducada"
        static let cancelButton             = "Cancelar"
        static let agreeButton              = "Aceptar"
        static let understoodButton         = "Entiendo"
        static let errorServiceParse        = "Problemas al extraer los datos"
        static let errorService             = "Problemas al extraer los datos"
        static let errorServiceNotFount     = "No hay respuesta del servicio"
    }
    
    struct Login {
        static let errorUser        = "Ingrese correctamente su documento"
        static let errorName        = "Ingrese correctamente su usuario"
        static let errorPassword    = "Ingrese correctamente su contraseña"
    }
    
    struct Loginregister {
        static let errorUser        = "Ingrese correctamente su usuario"
        static let errorName        = "Ingrese correctamente su usuario"
        static let errorPassword    = "Ingrese correctamente su contraseña"
    }
}
