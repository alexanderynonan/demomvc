//
//  ViewController.swift
//  Demo Nuevo
//
//  Created by Alexander Ynoñan H. on 4/01/22.
//

import UIKit

class ViewController: UIViewController {

//    DECLARACIONES DE Outlet y VARIABLES
    
    @IBOutlet weak private var lblName                  : UILabel!
    @IBOutlet weak private var txtName                  : UITextField!
    @IBOutlet weak private var txvName                  : UITextView!
    @IBOutlet weak private var tblUser                  : UITableView!
    @IBOutlet weak private var clvUse                   : UICollectionView!
    @IBOutlet weak private var scrlvUser                : UIScrollView!
    @IBOutlet weak private var constraintHeightUser     : NSLayoutConstraint!
    @IBOutlet weak private var btnUserTitle             : UIButton!
    @IBOutlet weak private var switchUser               : UISwitch!
    @IBOutlet weak private var imgUser                  : UIImageView!
    @IBOutlet weak private var viewHeader               : UIView!
    @IBOutlet weak private var pickerUser               : UIPickerView!
    
    
    var objUser : UserBE?
    var services = UserWS()
    
    
    private func loadLogin(){
        
        
        guard let dataUser = self.objUser else {
            print("No se ejecutara las demas lineas de codigo si 'dataUser' no contiene data")
            return
        }
        
        print(dataUser.name ?? "")
        print("Se ejectura cada linea de codigo si 'dataUser' tiene data")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    

}

